package domain.user.action

import com.nhaarman.mockitokotlin2.eq
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import domain.user.model.RegistrationData
import domain.user.model.UserService
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test

class RegisterUserTest {

    private lateinit var  userService: UserService
    private lateinit var  registerUser: RegisterUser

    private val username = "username"
    private val name = "name"
    private val registrationData = RegistrationData(username, name)

    @BeforeEach
    fun setUp(){
        userService = mock()
        registerUser = RegisterUser(userService)
    }

    @Test
    fun `should call user service passing the registration data`(){
        registerUser(username, name)
        verify(userService).create(eq(registrationData))
    }

}