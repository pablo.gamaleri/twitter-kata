package domain.user.action

import com.nhaarman.mockitokotlin2.eq
import com.nhaarman.mockitokotlin2.given
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import domain.user.model.User
import domain.user.model.UserService
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test

class UpdateNameTest {

    private lateinit var userService: UserService
    private lateinit var updateName: UpdateName

    private val username = "username"
    private val name = "name"
    private val user = User(username, name)
    private val newName = "newName"

    @BeforeEach
    fun setUp(){
        userService = mock()
        updateName = UpdateName(userService)
    }

    @Test
    fun `should call user service with actual name and new name`(){
        updateName(user.username, newName)
        verify(userService).updateName(eq(username), eq(newName))
    }

}