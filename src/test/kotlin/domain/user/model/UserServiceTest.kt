package domain.user.model

import com.nhaarman.mockitokotlin2.eq
import com.nhaarman.mockitokotlin2.given
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows

class UserServiceTest {

    private lateinit var  userRepository: UserRepository
    private lateinit var  userService: UserService

    private val username = "username"
    private val name = "name"
    private val registrationData = RegistrationData(username, name)
    private val user = User(username, name)
    private val newName = "newName"

    @BeforeEach
    fun setUp(){
        userRepository = mock()
        userService = DefaultUserService(userRepository)
    }

    @Test
    fun `should create a user given registration data`(){
        userService.create(registrationData)
        verify(userRepository).addUser(eq(user))
    }

    @Test
    fun `should throw an error when adding a user with an existing username`(){
        given(userRepository.existsByUsername(username)).willReturn(true)
        assertThrows<UsernameAlreadyInUse> { userService.create(registrationData) }
    }

    @Test
    fun `should update a user's real name`(){
        given(userRepository.existsByUsername(username)).willReturn(true)

        userService.updateName(username, newName)

        verify(userRepository).existsByUsername(eq(username))
        verify(userRepository).updateName(eq(username), eq(newName))
    }

    @Test
    fun `should throw error when user with given username doesn't exist`(){
        given(userRepository.existsByUsername(username)).willReturn(false)
        assertThrows<UserNotFound> { userService.updateName(username, newName) }
    }

}