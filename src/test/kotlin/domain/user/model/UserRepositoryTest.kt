package domain.user.model

import infrastructure.user.InMemoryUserRepository
import infrastructure.user.JedisRedisUserClient
import infrastructure.user.RedisUserRepository
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.api.assertThrows
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.MethodSource
import java.util.stream.Stream

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class UserRepositoryTest {

    private val username = "username"
    private val name = "name"
    private val user = User(username, name)
    private val newName = "newName"

    @ParameterizedTest
    @MethodSource("implementationDataSource")
    fun `should validate if a user with a given username exists`(userRepository: UserRepository){
        userRepository.addUser(user)
        assertTrue { userRepository.existsByUsername(username) }
    }

    @ParameterizedTest
    @MethodSource("implementationDataSource")
    fun `should find an existing user by username`(userRepository: UserRepository){
        userRepository.addUser(user)
        val actual = userRepository.findByUsername(username)
        assertEquals(user, actual)
    }

    @ParameterizedTest
    @MethodSource("implementationDataSource")
    fun `should update a user's name`(userRepository: UserRepository){
        userRepository.addUser(user)
        userRepository.updateName(username, newName = newName)
        val actual = userRepository.findByUsername(username)
        assertNotEquals(user, actual)
        assertEquals(user.username, actual?.username)
    }

    @ParameterizedTest
    @MethodSource("implementationDataSource")
    fun `should throw exception if given username doesn't match any user`(userRepository: UserRepository){
        assertThrows<UserNotFound> { userRepository.updateName("nonExistingUsername", newName = newName) }
    }

    private fun implementationDataSource() = Stream.of(
        InMemoryUserRepository(),
        RedisUserRepository(JedisRedisUserClient())
    )
}