package domain.follow.action

import com.nhaarman.mockitokotlin2.given
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import domain.follow.model.FollowService
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test

class ListUserFolloweesTest {

    private lateinit var listUserFollowees: ListUserFollowees
    private lateinit var followService: FollowService

    private val username = "username"
    private val followeeList = listOf<String>()
    private var result: List<String>? = null


    @BeforeEach
    internal fun setUp() {
        followService = mock()
        listUserFollowees = ListUserFollowees(followService)
    }

    @Test
    fun `call service with given username`(){
        givenAFollowCollection()

        whenListingUserFollowees()

        verifyServiceIsCalled()
        Assertions.assertEquals(followeeList, result)
    }

    private fun whenListingUserFollowees() {
        result = listUserFollowees.invoke(username)
    }

    private fun verifyServiceIsCalled() {
        verify(followService).followeesByUsername(username)
    }

    private fun ListUserFolloweesTest.givenAFollowCollection() {
        given(followService.followeesByUsername(username)).willReturn(followeeList)
    }
}