package domain.follow.action

import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import domain.follow.model.FollowService
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test

class FollowUserTest {

    private lateinit var followService: FollowService
    private lateinit var followUser: FollowUser

    private val followerUsername = "follower"
    private val toBeFollowedUsername = "followee"

    @BeforeEach
    fun setUp() {
        followService = mock()
        followUser = FollowUser(followService)
    }

    @Test
    fun `should call user service with given follower and followee usernames`() {
        followUser(followerUsername, toBeFollowedUsername)
        verify(followService).follow(followerUsername, toBeFollowedUsername)
    }
}