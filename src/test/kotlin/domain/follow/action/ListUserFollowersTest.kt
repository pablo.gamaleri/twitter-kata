package domain.follow.action

import com.nhaarman.mockitokotlin2.given
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import domain.follow.model.FollowService
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test

class ListUserFollowersTest {

    private lateinit var listUserFollowers: ListUserFollowers
    private lateinit var followService: FollowService

    private val username = "username"
    private val followerList = listOf<String>()
    private var result: List<String>? = null


    @BeforeEach
    internal fun setUp() {
        followService = mock()
        listUserFollowers = ListUserFollowers(followService)
    }

    @Test
    fun `call service with given username`(){
        givenAFollowCollection()

        whenListingUserFollowers()

        verifyServiceIsCalled()
        assertEquals(followerList, result)
    }

    private fun whenListingUserFollowers() {
        result = listUserFollowers.invoke(username)
    }

    private fun verifyServiceIsCalled() {
        verify(followService).followersByUsername(username)
    }

    private fun ListUserFollowersTest.givenAFollowCollection() {
        given(followService.followersByUsername(username)).willReturn(followerList)
    }
}