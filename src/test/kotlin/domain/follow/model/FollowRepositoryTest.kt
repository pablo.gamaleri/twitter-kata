package domain.follow.model

import infrastructure.database.JedisConnectionPool
import infrastructure.follow.InMemoryFollowRepository
import infrastructure.follow.JedisRedisFollowClient
import infrastructure.follow.RedisFollowRepository
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.MethodSource
import java.util.stream.Stream

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class FollowRepositoryTest {

    private val username = "username"
    private val followerUsername = "followerUsername"

    @BeforeEach
    internal fun setUp() {
        JedisConnectionPool.jedis().use { it.flushDB() }
    }

    @ParameterizedTest
    @MethodSource("implementationDataSource")
    fun `should add a follower and find an existing follower collection with it`(followRepository: FollowRepository){
        val expected = givenAFollowerCollectionWithAFollower(followRepository)
        val actual = followRepository.findByUsername(username)
        assertEquals(expected, actual)
    }

    @ParameterizedTest
    @MethodSource("implementationDataSource")
    fun `should add a followee and find an existing follower collection with it`(followRepository: FollowRepository){
        val expected = givenAFollowerCollectionWithAFollowee(followRepository)
        val actual = followRepository.findByUsername(username)
        assertEquals(expected, actual)
    }

    private fun givenAFollowerCollectionWithAFollower(followRepository: FollowRepository): FollowCollection {
        val expected = FollowCollection(username, listOf(followerUsername))
        followRepository.add(expected)
        return expected
    }

    private fun givenAFollowerCollectionWithAFollowee(followRepository: FollowRepository): FollowCollection {
        val expected = FollowCollection(username, followees = listOf(followerUsername))
        followRepository.add(expected)
        return expected
    }

    private fun implementationDataSource() = Stream.of(
        InMemoryFollowRepository(),
        RedisFollowRepository(JedisRedisFollowClient())
    )
}