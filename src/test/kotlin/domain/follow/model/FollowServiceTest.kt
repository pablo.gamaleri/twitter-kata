package domain.follow.model

import com.nhaarman.mockitokotlin2.eq
import com.nhaarman.mockitokotlin2.given
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import domain.user.model.User
import domain.user.model.UserService
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows

class FollowServiceTest {

    private lateinit var  followRepository: FollowRepository
    private lateinit var  followService: FollowService
    private lateinit var  userService: UserService

    private val followerUsername = "followerUsername"
    private val toBeFollowedUsername = "toBeFollowedUsername"
    private val toBeFollowedName = "toBeFollowedName"
    private val username = "username"
    private val userFollowedByFollower = User(toBeFollowedUsername, toBeFollowedName)

    @BeforeEach
    fun setUp(){
        userService = mock()
        followRepository = mock()
        followService = DefaultFollowService(userService, followRepository)
    }

    @Test
    fun `should call user service and repository`() {
        givenTwoExistingUsersWithoutFollows()

        whenFollowingAUser()

        thenVerifyUserServiceCalls()
        thenVerifyRepositoryCalls()
    }

    @Test
    fun `should throw when already following a user`() {
        givenAFollowerUser()
        givenAUserFollowedByFollower()

        assertThrows<AlreadyFollowingUser> { whenFollowingAUser() }
    }

    @Test
    fun `should call the user service to validate user exists and repository to get follows`() {
        givenAUser()

        whenListingFollowersOfAUser()

        thenVerifyUserExists()
        thenCallRepositoryToGetFollows()
    }

    private fun thenVerifyUserExists() {
        verify(userService).existsByUsername(username)
    }

    private fun thenCallRepositoryToGetFollows() {
        verify(followRepository).findByUsername(username)
    }

    private fun whenListingFollowersOfAUser() {
        followService.followersByUsername(username)
    }

    private fun givenTwoExistingUsersWithoutFollows() {
        given(userService.existsByUsername(followerUsername)).willReturn(true)
        given(userService.existsByUsername(toBeFollowedUsername)).willReturn(true)
        val emptyCollection = FollowCollection(toBeFollowedUsername, emptyList())
        given(followRepository.findByUsername(toBeFollowedUsername)).willReturn(emptyCollection)
    }

    private fun givenAFollowerUser() {
        given(userService.existsByUsername(followerUsername)).willReturn(true)
    }

    private fun givenAUserFollowedByFollower() {
        given(userService.existsByUsername(userFollowedByFollower.username)).willReturn(true)
        val collectionWithAFollower = FollowCollection(userFollowedByFollower.username, listOf(followerUsername))
        given(followRepository.findByUsername(userFollowedByFollower.username)).willReturn(collectionWithAFollower)
    }

    private fun givenAUser() {
        given(userService.existsByUsername(username)).willReturn(true)
    }

    private fun thenVerifyUserServiceCalls() {
        verify(userService).existsByUsername(eq(followerUsername))
        verify(userService).existsByUsername(eq(toBeFollowedUsername))
    }

    private fun thenVerifyRepositoryCalls() {
        verify(followRepository).findByUsername(eq(toBeFollowedUsername))
        val followerCollection = FollowCollection(toBeFollowedUsername, listOf(followerUsername))
        verify(followRepository).add(eq(followerCollection))
    }

    private fun whenFollowingAUser() {
        followService.follow(followerUsername, toBeFollowedUsername)
    }
}