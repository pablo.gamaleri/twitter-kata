package domain.tweet.action

import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import domain.tweet.model.TweetService
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test

class ListUserTweetsTest {

    private val username = "username"


    private lateinit var tweetService: TweetService
    private lateinit var listUserTweets: ListUserTweets

    @BeforeEach
    internal fun setUp() {
        tweetService = mock()
        listUserTweets = ListUserTweets(tweetService)
    }

    @Test
    internal fun `should call tweet service`() {
        whenListingUserTweets()
        verifyServiceIsCalled()
    }

    private fun whenListingUserTweets() {
        listUserTweets(username)
    }

    private fun verifyServiceIsCalled() {
        verify(tweetService).list(username)
    }
}