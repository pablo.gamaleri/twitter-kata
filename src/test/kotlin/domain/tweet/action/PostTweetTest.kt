package domain.tweet.action

import com.nhaarman.mockitokotlin2.eq
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import domain.tweet.model.Tweet
import domain.tweet.model.TweetService
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test

class PostTweetTest {

    private lateinit var postTweet: PostTweet
    private lateinit var tweetService: TweetService

    private val username = "username"
    private val message = "message"
    private val tweet = Tweet(username, message)

    @BeforeEach
    internal fun setUp() {
        tweetService = mock()
        postTweet = PostTweet(tweetService)
    }

    @Test
    internal fun `should call tweet service`() {
        whenAUserTweets()
        verifyTweetServiceIsCalledWithParams()
    }

    private fun verifyTweetServiceIsCalledWithParams() {
        verify(tweetService).post(eq(tweet.username), eq(tweet.message))
    }

    private fun whenAUserTweets() {
        postTweet(tweet.username, tweet.message)
    }
}