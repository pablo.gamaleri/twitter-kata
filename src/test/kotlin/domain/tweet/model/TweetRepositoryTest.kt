package domain.tweet.model

import infrastructure.database.JedisConnectionPool
import infrastructure.tweet.InMemoryTweetRepository
import infrastructure.tweet.JedisRedisTweetClient
import infrastructure.tweet.RedisTweetRepository
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.MethodSource
import java.util.stream.Stream

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class TweetRepositoryTest {

    private val message = "message"
    private val username = "username"
    private val tweet = Tweet(username, message)
    private val listWithOneTweet = listOf(tweet)

    @BeforeAll
    internal fun setUp() {
        JedisConnectionPool.jedis().use { it.flushDB() }
    }

    @ParameterizedTest
    @MethodSource("implementationSource")
    internal fun `should add tweet`(tweetRepository: TweetRepository) {
        tweetRepository.add(tweet)
        val actualList = tweetRepository.list(username)
        assertEquals(listWithOneTweet, actualList)
    }

    private fun implementationSource() = Stream.of(
        InMemoryTweetRepository(),
        RedisTweetRepository(JedisRedisTweetClient())
    )
}