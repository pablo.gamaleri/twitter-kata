package domain.tweet.model

import com.nhaarman.mockitokotlin2.eq
import com.nhaarman.mockitokotlin2.given
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import domain.user.model.UserNotFound
import domain.user.model.UserService
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows

class TweetServiceTest {

    private lateinit var tweetService: TweetService
    private lateinit var userService: UserService
    private lateinit var tweetRepository: TweetRepository

    private val username = "username"
    private val message = "message"
    private val tweet = Tweet(username, message)
    private val listOfOneTweet = listOf(message)

    @BeforeEach
    internal fun setUp() {
        userService = mock()
        tweetRepository = mock()
        tweetService = DefaultTweetService(tweetRepository, userService)
    }

    @Test
    internal fun `should create Tweet object and call repository when tweeting`() {
        givenExistingUser()
        whenTweeting()
        verifyUserServiceIsCalled()
        verifyRepositoryIsCalled()
    }

    @Test
    internal fun `should throw if username does not exist when tweeting`() {
        givenInexistentUser()
        assertThrows<UserNotFound> { whenTweeting() }
    }

    @Test
    internal fun `should return the tweets of a user`() {
        givenExistingUserWithTweets()

        val actualTweets = whenListingUserTweets()

        verifyUserExists()
        verifyRepositoryIsCalledForList()
        Assertions.assertEquals(listOfOneTweet, actualTweets)
    }

    @Test
    internal fun `should throw if user does not exist when listing user tweets`() {
        givenInexistingUser()
        assertThrows<UserNotFound> { whenListingUserTweets() }
    }

    private fun givenInexistingUser() {
        given(userService.existsByUsername(username)).willReturn(false)
    }

    private fun givenExistingUserWithTweets() {
        given(tweetRepository.list(username)).willReturn(listOf(tweet))
        given(userService.existsByUsername(username)).willReturn(true)
    }

    private fun verifyUserExists() {
        verify(userService).existsByUsername(username)
    }

    private fun verifyRepositoryIsCalledForList() {
        verify(tweetRepository).list(username)
    }

    private fun whenListingUserTweets(): List<String> {
        return tweetService.list(username)
    }

    private fun whenTweeting() {
        tweetService.post(username, message)
    }

    private fun givenInexistentUser() {
        given(userService.existsByUsername(username)).willReturn(false)
    }

    private fun givenExistingUser() {
        given(userService.existsByUsername(username)).willReturn(true)
    }

    private fun verifyRepositoryIsCalled() {
        verify(tweetRepository).add(eq(tweet))
    }

    private fun verifyUserServiceIsCalled(){
        verify(userService).existsByUsername(eq(username))
    }
}