import io.restassured.module.kotlin.extensions.Given
import io.restassured.module.kotlin.extensions.Then
import io.restassured.module.kotlin.extensions.When
import org.junit.jupiter.api.AfterAll
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class UserControllerTest {

    private lateinit var app: App

    @BeforeAll
    internal fun setUp() {
        app = App()
        app.start(8080)
    }

    @AfterAll
    internal fun tearDown() {
        app.stop()
    }

    @Test
    internal fun `should register a new user`() {
        Given {
            param("username", "username")
            param("name", "name")
        } When {
            post("/users")
        } Then {
            statusCode(201)
        }
    }
}