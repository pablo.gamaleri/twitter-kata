import domain.follow.action.FollowUser
import domain.follow.action.ListUserFollowees
import domain.follow.action.ListUserFollowers
import domain.follow.model.AlreadyFollowingUser
import domain.follow.model.DefaultFollowService
import domain.follow.model.FollowRepository
import domain.follow.model.FollowService
import domain.tweet.action.ListUserTweets
import domain.tweet.action.PostTweet
import domain.tweet.model.DefaultTweetService
import domain.tweet.model.TweetRepository
import domain.tweet.model.TweetService
import domain.user.action.RegisterUser
import domain.user.action.UpdateName
import domain.user.model.*
import infrastructure.database.JedisConnectionPool
import infrastructure.follow.JedisRedisFollowClient
import infrastructure.follow.RedisFollowRepository
import infrastructure.tweet.JedisRedisTweetClient
import infrastructure.tweet.RedisTweetRepository
import infrastructure.user.JedisRedisUserClient
import infrastructure.user.RedisUserRepository
import io.javalin.Javalin
import io.javalin.apibuilder.ApiBuilder.*
import io.javalin.http.Context
import io.javalin.plugin.json.JavalinJson
import java.lang.RuntimeException

fun main() {
    App().start(8080)
}

class App {

    private val userRepository: UserRepository
    private val userService: UserService
    private val registerUser: RegisterUser
    private val updateName: UpdateName

    private val followRepository: FollowRepository
    private val followService: FollowService
    private val listUserFollowers: ListUserFollowers
    private val listUserFollowees: ListUserFollowees
    private val followUser: FollowUser

    private val tweetRepository: TweetRepository
    private val tweetService: TweetService
    private val listUserTweets: ListUserTweets
    private val postTweet: PostTweet

    init {
        JedisConnectionPool.jedis().use { it.flushAll() }
        userRepository = RedisUserRepository(JedisRedisUserClient())
        userService = DefaultUserService(userRepository)
        registerUser = RegisterUser(userService)
        updateName = UpdateName(userService)

        followRepository = RedisFollowRepository(JedisRedisFollowClient())
        followService = DefaultFollowService(userService, followRepository)
        listUserFollowers = ListUserFollowers(followService)
        listUserFollowees = ListUserFollowees(followService)
        followUser = FollowUser(followService)

        tweetRepository = RedisTweetRepository(JedisRedisTweetClient())
        tweetService = DefaultTweetService(tweetRepository, userService)
        listUserTweets = ListUserTweets(tweetService)
        postTweet = PostTweet(tweetService)
    }

    fun start(port: Int){
        app.start(port)
    }

    fun stop(){
        app.stop()
    }

    private val app: Javalin = Javalin.create().routes {
        post("/users") { registerNewUser(it) }
        put("/users/:username") { updateUserName(it) }
        post("/followers/:username") { followUser(it) }
        get("/followers/:username") { listUserFollowers(it) }
        get("/followees/:username") { listUserFollowees(it) }
        post("/tweets/:username") { postUserTweet(it) }
        get("/tweets/:username") { listUserTweets(it) }
    }

    private fun registerNewUser(context: Context) {
        try {
            registerUser.invoke(
                context.formParam("username")!!,
                context.formParam("name")!!
            )
            context.status(201)
        } catch (error: UsernameAlreadyInUse){
            respondWithError(context, error, 400)
        }
    }

    private fun updateUserName(context: Context) {
        try{
            updateName.invoke(
                context.pathParam("username"),
                context.formParam("newName")!!
            )
        } catch (error: UserNotFound){
            respondWithError(context, error, 400)
        }
    }

    private fun followUser(context: Context) {
        try{
            followUser.invoke(
                context.formParam("followerUsername")!!,
                context.pathParam("username")
            )
        } catch (error: UserNotFound){
            respondWithError(context, error, 400)
        } catch (error: AlreadyFollowingUser){
            respondWithError(context, error, 400)
        }
    }

    private fun listUserFollowers(context: Context) {
        try{
            val followers = listUserFollowers.invoke(context.pathParam("username"))
            context.result(toJson(followers))
        } catch (error: UserNotFound){
            respondWithError(context, error, 400)
        }
    }

    private fun listUserFollowees(context: Context) {
        try{
            val followees = listUserFollowees.invoke(context.pathParam("username"))
            context.result(toJson(followees))
        } catch (error: UserNotFound){
            respondWithError(context, error, 400)
        }
    }

    private fun postUserTweet(context: Context) {
        try{
            postTweet.invoke(
                context.pathParam("username"),
                context.formParam("message")!!
            )
            context.status(201)
        } catch (error: UserNotFound){
            respondWithError(context, error, 400)
        }
    }

    private fun listUserTweets(context: Context) {
        try{
            val tweets = listUserTweets.invoke(context.pathParam("username"))
            context.result(toJson(tweets))
        } catch (error: UserNotFound){
            respondWithError(context, error, 400)
        }
    }

    private fun respondWithError(context: Context, error: RuntimeException, statusCode: Int) {
        context.status(statusCode)
        context.result(toJson(Error(error.message!!)))
    }

    private fun toJson(any: Any) = JavalinJson.toJson(any)

    inner class Error(val message: String)
}