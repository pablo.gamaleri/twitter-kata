package infrastructure.follow

import domain.follow.model.FollowCollection
import infrastructure.database.JedisConnectionPool

class JedisRedisFollowClient: RedisFollowClient {

    override fun add(followCollection: FollowCollection) {
        addFollows(followCollection)
    }

    override fun findByUsername(key: String): FollowCollection? {
        val followers = getList(toRedisFollowerSetKey(key))
        val followees = getList(toRedisFolloweeSetKey(key))
        return if(followers != null){
            if(followees != null) FollowCollection(key, followers, followees) else FollowCollection(key, followers)
        } else {
            if(followees != null) FollowCollection(key, followees = followees) else null
        }
    }

    private fun addFollows(followCollection: FollowCollection) {
        val followerKey = toRedisFollowerSetKey(followCollection.userId)
        for (follower in followCollection.followers) {
            setAdd(followerKey, follower)
        }

        val followeeKey = toRedisFolloweeSetKey(followCollection.userId)
        for (follower in followCollection.followees) {
            setAdd(followeeKey, follower)
        }
    }

    private fun toRedisFollowerSetKey(username: String) = "$username-followers"

    private fun toRedisFolloweeSetKey(username: String) = "$username-followees"

    private fun setAdd(key: String, value: String) {
        JedisConnectionPool.jedis().use {
            it.sadd(key, value)
        }
    }

    private fun getList(key: String): List<String>? {
        JedisConnectionPool.jedis().use {
            return it.smembers(key)?.toList()
        }
    }
}