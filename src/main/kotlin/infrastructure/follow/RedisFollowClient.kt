package infrastructure.follow

import domain.follow.model.FollowCollection

interface RedisFollowClient {

    fun add(followCollection: FollowCollection)
    fun findByUsername(key: String): FollowCollection?
}
