package infrastructure.follow

import domain.follow.model.FollowCollection
import domain.follow.model.FollowRepository

class RedisFollowRepository(
        private val redisFollowClient: RedisFollowClient
) : FollowRepository {

    override fun add(followCollection: FollowCollection) {
        redisFollowClient.add(followCollection)
    }

    override fun findByUsername(username: String): FollowCollection? {
        return redisFollowClient.findByUsername(username)
    }
}