package infrastructure.follow

import domain.follow.model.FollowCollection
import domain.follow.model.FollowRepository

class InMemoryFollowRepository: FollowRepository {

    private val followCollectionList: ArrayList<FollowCollection> = arrayListOf()

    override fun add(followCollection: FollowCollection) {
        followCollectionList.removeIf { it.userId == followCollection.userId}
        followCollectionList.add(followCollection)
    }

    override fun findByUsername(username: String): FollowCollection? {
        return followCollectionList.find { it.userId == username }
    }
}
