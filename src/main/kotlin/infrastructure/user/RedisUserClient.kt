package infrastructure.user

import domain.user.model.User

interface RedisUserClient {
    fun add(user: User)
    fun existsByUsername(username: String): Boolean
    fun findByUsername(username: String): User?
    fun updateName(username: String, newName: String)
}