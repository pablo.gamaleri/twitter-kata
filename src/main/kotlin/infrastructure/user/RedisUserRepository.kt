package infrastructure.user

import domain.user.model.User
import domain.user.model.UserNotFound
import domain.user.model.UserRepository

class RedisUserRepository(
    private val redisUserClient: RedisUserClient
): UserRepository {

    override fun addUser(user: User) {
        redisUserClient.add(user)
    }

    override fun existsByUsername(username: String): Boolean {
        return redisUserClient.existsByUsername(username)
    }

    override fun updateName(username: String, newName: String) {
        validateUserExists(username)
        redisUserClient.updateName(username, newName)
    }

    override fun findByUsername(username: String): User? {
        return redisUserClient.findByUsername(username)
    }

    private fun validateUserExists(username: String) {
        if(!existsByUsername(username)){
            throw UserNotFound()
        }
    }
}