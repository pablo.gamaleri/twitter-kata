package infrastructure.user

import domain.user.model.User
import infrastructure.database.JedisConnectionPool

class JedisRedisUserClient: RedisUserClient {

    override fun add(user: User) {
        setUser(user)
    }

    override fun existsByUsername(username: String): Boolean {
        return getName(username) != null
    }

    override fun findByUsername(username: String): User? {
        return getUser(username)
    }

    override fun updateName(username: String, newName: String) {
        updateUserName(username, newName)
    }

    private fun getName(key: String): String? {
        JedisConnectionPool.jedis().use {
            return it.hget(key, "name")
        }
    }

    private fun getUser(key: String): User? {
        val name = getName(key)
        return if(name != null) User(key, name) else null
    }

    private fun setUser(user: User){
        JedisConnectionPool.jedis().use {
            it.hset(user.username, "name", user.name)
        }
    }

    private fun updateUserName(username: String, newName: String){
        JedisConnectionPool.jedis().use {
            it.hset(username, "name", newName)
        }
    }
}