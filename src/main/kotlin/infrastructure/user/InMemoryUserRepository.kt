package infrastructure.user

import domain.user.model.User
import domain.user.model.UserNotFound
import domain.user.model.UserRepository
import domain.user.model.UsernameAlreadyInUse

class InMemoryUserRepository(
    private val users: ArrayList<User> = ArrayList()
): UserRepository {

    override fun addUser(user: User) {
        users.add(user)
    }

    override fun existsByUsername(username: String): Boolean {
        return users.any { it.username == username }
    }

    override fun updateName(username: String, newName: String) {
        val user = getUser(username)
        users[users.indexOf(user)] = User(user.username, newName)
    }

    override fun findByUsername(username: String): User? {
        return users.find { it.username == username }
    }

    private fun getUser(username: String): User {
        val user = findByUsername(username)
        validateFound(user)
        return user!!
    }

    private fun validateFound(user: User?) {
        if (user == null) {
            throw UserNotFound()
        }
    }
}