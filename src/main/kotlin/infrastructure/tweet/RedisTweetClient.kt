package infrastructure.tweet

import domain.tweet.model.Tweet

interface RedisTweetClient {
    fun add(tweet: Tweet)
    fun list(username: String): List<Tweet>
}
