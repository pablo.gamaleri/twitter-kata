package infrastructure.tweet

import domain.tweet.model.Tweet
import infrastructure.database.JedisConnectionPool

class JedisRedisTweetClient(
    private val listKey: String = "tweets",
    private val delimiter: String = "-*-"
): RedisTweetClient {

    override fun add(tweet: Tweet) {
        listAdd(tweet)
    }

    override fun list(username: String): List<Tweet> {
        return listFor(username)
    }

    private fun listAdd(tweet: Tweet){
        JedisConnectionPool.jedis().use {
            it.rpush(listKey, toString(tweet))
        }
    }

    private fun listFor(username: String): List<Tweet> {
        val list = listAll()
        return list.map { toTweet(it) }.filter { it.username == username }
    }

    private fun listAll(): MutableList<String> {
        JedisConnectionPool.jedis().use {
            val length = it.llen(listKey)!!
            return it.lrange(listKey,0, length)!!
        }
    }

    private fun toString(tweet: Tweet): String {
        return tweet.username + delimiter + tweet.message
    }

    private fun toTweet(string: String): Tweet {
        val username = string.substringBefore(delimiter)
        val message = string.substringAfter(delimiter)
        return Tweet(username, message)
    }
}
