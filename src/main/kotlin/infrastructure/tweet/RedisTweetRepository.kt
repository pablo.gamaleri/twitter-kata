package infrastructure.tweet

import domain.tweet.model.Tweet
import domain.tweet.model.TweetRepository

class RedisTweetRepository(
    private val redisTweetClient: RedisTweetClient
): TweetRepository {

    override fun add(tweet: Tweet) {
        redisTweetClient.add(tweet)
    }

    override fun list(username: String): List<Tweet> {
        return redisTweetClient.list(username)
    }
}