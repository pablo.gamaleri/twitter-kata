package infrastructure.tweet

import domain.tweet.model.Tweet
import domain.tweet.model.TweetRepository

class InMemoryTweetRepository: TweetRepository {

    private val tweets: ArrayList<Tweet> = ArrayList()

    override fun add(tweet: Tweet) {
        tweets.add(tweet)
    }

    override fun list(username: String): List<Tweet> {
        return tweets.filter { it.username == username }
    }
}
