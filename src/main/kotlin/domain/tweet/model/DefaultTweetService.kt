package domain.tweet.model

import domain.user.model.UserNotFound
import domain.user.model.UserService

class DefaultTweetService(
    private val tweetRepository: TweetRepository,
    private val userService: UserService
) : TweetService {

    override fun post(username: String, message: String) {
        validateUserExists(username)
        tweetRepository.add(Tweet(username, message))
    }

    override fun list(username: String): List<String> {
        validateUserExists(username)
        return findTweetMessages(username)
    }

    private fun validateUserExists(username: String) {
        if (!userService.existsByUsername(username)) {
            throw UserNotFound()
        }
    }

    private fun findTweetMessages(username: String): List<String> {
        val messages = ArrayList<String>()
        tweetRepository.list(username).forEach { messages.add(it.message) }
        return messages
    }
}