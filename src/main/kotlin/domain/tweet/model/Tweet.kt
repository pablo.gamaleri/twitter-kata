package domain.tweet.model

data class Tweet(val username: String, val message: String)
