package domain.tweet.model

import domain.tweet.model.Tweet

interface TweetRepository {
    fun add(tweet: Tweet)
    fun list(username: String): List<Tweet>
}
