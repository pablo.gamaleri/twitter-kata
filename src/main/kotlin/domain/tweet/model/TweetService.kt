package domain.tweet.model

interface TweetService {
    fun post(username: String, message: String)
    fun list(username: String): List<String>
}