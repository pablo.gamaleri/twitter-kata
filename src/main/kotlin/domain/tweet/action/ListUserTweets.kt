package domain.tweet.action

import domain.tweet.model.TweetService

class ListUserTweets(private val tweetService: TweetService) {

    operator fun invoke(username: String): List<String> {
        return tweetService.list(username)
    }
}
