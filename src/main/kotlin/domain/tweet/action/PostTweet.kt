package domain.tweet.action

import domain.tweet.model.TweetService

class PostTweet(private val tweetService: TweetService) {

    operator fun invoke(username: String, message: String) {
        tweetService.post(username, message)
    }
}