package domain.follow.model

interface FollowService {
    fun follow(followerUsername: String, followeeUsername: String)
    fun followersByUsername(username: String): List<String>
    fun followeesByUsername(username: String): List<String>
}
