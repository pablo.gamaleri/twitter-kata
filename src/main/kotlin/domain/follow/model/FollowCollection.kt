package domain.follow.model

data class FollowCollection(
    val userId: String,
    val followers: List<String> = emptyList(),
    val followees: List<String> = emptyList()
)