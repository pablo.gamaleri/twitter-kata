package domain.follow.model

import domain.user.model.UserNotFound
import domain.user.model.UserService

class DefaultFollowService(
    private val userService: UserService,
    private val followRepository: FollowRepository
): FollowService {

    override fun follow(followerUsername: String, followeeUsername: String) {
        validateBothUsersExist(followerUsername, followeeUsername)
        val followeeFollowCollection = find(followeeUsername)
        val followerFollowCollection = find(followerUsername)
        validateNotFollowing(followeeFollowCollection, followerUsername)
        updateFollowers(followeeFollowCollection, followerUsername)
        updateFollowees(followerFollowCollection, followeeUsername)
    }

    override fun followersByUsername(username: String): List<String> {
        return followCollection(username).followers
    }

    override fun followeesByUsername(username: String): List<String> {
        return followCollection(username).followees
    }

    private fun validateUserExists(username: String) {
        if(!userService.existsByUsername(username)){
            throw UserNotFound()
        }
    }

    private fun validateBothUsersExist(followerUsername: String, toBeFollowedUsername: String) {
        if(!userService.existsByUsername(followerUsername)
            || !userService.existsByUsername(toBeFollowedUsername)){
            throw UserNotFound()
        }
    }

    private fun validateNotFollowing(followCollection: FollowCollection, followerUsername: String) {
        if (followCollection.followers.contains(followerUsername)) {
            throw AlreadyFollowingUser()
        }
    }

    private fun find(username: String) = followRepository.findByUsername(username) ?: FollowCollection(username)

    private fun updateFollowers(
        followeefollowCollection: FollowCollection,
        followerUsername: String
    ) {
        val updatedFolloweeFollowerList: ArrayList<String> = ArrayList(followeefollowCollection.followers)
        updatedFolloweeFollowerList.add(followerUsername)
        val updatedFolloweeFollowCollection = FollowCollection(
            followeefollowCollection.userId,
            updatedFolloweeFollowerList,
            followeefollowCollection.followees
        )
        followRepository.add(updatedFolloweeFollowCollection)
    }

    private fun updateFollowees(
        followerFollowCollection: FollowCollection,
        followeeUsername: String
    ) {
        val updatedFollowerFolloweeList: ArrayList<String> = ArrayList(followerFollowCollection.followees)
        updatedFollowerFolloweeList.add(followeeUsername)
        val updatedFollowerFollowCollection = FollowCollection(
            followerFollowCollection.userId,
            followerFollowCollection.followers,
            updatedFollowerFolloweeList
        )
        followRepository.add(updatedFollowerFollowCollection)
    }

    private fun followCollection(username: String): FollowCollection {
        validateUserExists(username)
        return find(username)
    }
}