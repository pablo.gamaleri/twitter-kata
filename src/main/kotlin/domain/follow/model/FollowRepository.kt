package domain.follow.model

interface FollowRepository {
    fun add(followCollection: FollowCollection)
    fun findByUsername(username: String): FollowCollection?
}
