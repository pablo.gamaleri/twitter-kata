package domain.follow.model

class AlreadyFollowingUser : RuntimeException(){
    override val message = "Already following user"
}
