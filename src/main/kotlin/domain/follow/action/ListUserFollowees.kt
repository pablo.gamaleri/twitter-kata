package domain.follow.action

import domain.follow.model.FollowService

class ListUserFollowees(private val followService: FollowService) {

    operator fun invoke(username: String): List<String> {
        return followService.followeesByUsername(username)
    }
}
