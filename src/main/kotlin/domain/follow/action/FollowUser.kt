package domain.follow.action

import domain.follow.model.FollowService

class FollowUser(private val followService: FollowService) {

    operator fun invoke(followerUsername: String, toBeFollowedUsername: String) {
        followService.follow(followerUsername, toBeFollowedUsername)
    }
}
