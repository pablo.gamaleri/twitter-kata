package domain.user.action

import domain.user.model.UserService

class UpdateName(private val userService: UserService){

    operator fun invoke(username: String, newName: String) {
        userService.updateName(username, newName)
    }
}
