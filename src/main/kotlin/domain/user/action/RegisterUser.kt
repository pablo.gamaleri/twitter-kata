package domain.user.action

import domain.user.model.RegistrationData
import domain.user.model.User
import domain.user.model.UserService

class RegisterUser(private val userService: UserService) {

    operator fun invoke(username: String, name: String) {
        val registrationData = RegistrationData(username, name)
        userService.create(registrationData)
    }
}