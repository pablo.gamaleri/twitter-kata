package domain.user.model

data class RegistrationData(val username: String, val name: String)
