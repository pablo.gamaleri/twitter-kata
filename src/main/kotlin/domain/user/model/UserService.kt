package domain.user.model

interface UserService {
    fun create(registrationData: RegistrationData)
    fun updateName(username: String, name: String)
    fun existsByUsername(username: String): Boolean
}
