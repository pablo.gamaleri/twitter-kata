package domain.user.model

import java.lang.RuntimeException

class UserNotFound : RuntimeException() {
    override val message = "User not found"
}
