package domain.user.model

class DefaultUserService(private val userRepository: UserRepository) : UserService {

    override fun create(registrationData: RegistrationData) {
        validateUsernameIsAvailable(registrationData.username)
        val user = userFrom(registrationData)
        userRepository.addUser(user)
    }

    override fun updateName(username: String, name: String) {
        validateUserExists(username)
        userRepository.updateName(username, newName = name)
    }

    override fun existsByUsername(username: String): Boolean {
        return userRepository.existsByUsername(username)
    }

    private fun validateUsernameIsAvailable(username: String) {
        if (userRepository.existsByUsername(username)) {
            throw UsernameAlreadyInUse()
        }
    }

    private fun userFrom(registrationData: RegistrationData): User {
        return User(registrationData.username, registrationData.name)
    }

    private fun validateUserExists(username: String) {
        if (!userRepository.existsByUsername(username)) {
            throw UserNotFound()
        }
    }
}