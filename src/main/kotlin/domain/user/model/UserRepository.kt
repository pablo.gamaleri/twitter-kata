package domain.user.model

interface UserRepository {
    fun addUser(user: User)
    fun existsByUsername(username: String): Boolean
    fun updateName(username: String, newName: String)
    fun findByUsername(username: String): User?
}