package domain.user.model

data class User(val username: String, val name: String)