package domain.user.model

class UsernameAlreadyInUse: RuntimeException(){
    override val message = "Username is already in use"
}